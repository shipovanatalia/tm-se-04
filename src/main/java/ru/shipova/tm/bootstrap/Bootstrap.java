package ru.shipova.tm.bootstrap;

import ru.shipova.tm.constant.CommandsConst;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.TaskService;

import java.util.Scanner;

public class Bootstrap {
    private ProjectRepository projectRepository;
    private ProjectService projectService;
    private TaskRepository taskRepository;
    private TaskService taskService;

    public static Scanner in;

    /**
     * Класс загрузчика приложения
     */
    public Bootstrap() {
        this.projectRepository = new ProjectRepository();
        this.projectService = new ProjectService();
        this.taskRepository = new TaskRepository();
        this.taskService = new TaskService();

        taskService.setTaskRepository(this.taskRepository);
        taskService.setProjectRepository(this.projectRepository);
        projectService.setProjectRepository(this.projectRepository);
        projectService.setTaskRepository(this.taskRepository);
    }

    public void init (){
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        in = new Scanner(System.in);

        String taskName;
        String projectName;

        while (true) {
            String input = in.nextLine();

            switch (input) {
                case CommandsConst.HELP:
                    System.out.println("help: Show all commands");
                    projectService.printAllCommands();
                    taskService.printAllCommands();
                    System.out.println("exit: exit from application");
                    break;
                case CommandsConst.CREATE_PROJECT:
                    System.out.println("[PROJECT CREATE]");
                    System.out.println("ENTER NAME:");
                    projectName = in.nextLine();
                    projectService.create(projectName);
                    System.out.println("[OK]");
                    break;
                case CommandsConst.LIST_OF_PROJECTS:
                    System.out.println("[PROJECT LIST]");
                    projectService.list();
                    break;
                case CommandsConst.CLEAR_ALL_PROJECTS:
                    projectService.clear();
                    System.out.println("[ALL PROJECTS REMOVED]");
                    break;
                case CommandsConst.REMOVE_PROJECT:
                    System.out.println("ENTER NAME:");
                    projectName = in.nextLine();
                    projectService.remove(projectName);
                    System.out.println("[PROJECT " + projectName + " REMOVED]");
                    break;
                case CommandsConst.CREATE_TASK:
                    System.out.println("[TASK CREATE]");
                    System.out.println("ENTER NAME:");
                    taskName = in.nextLine();
                    System.out.println("ENTER PROJECT NAME:");
                    projectName = in.nextLine();
                    taskService.create(taskName, projectName);
                    System.out.println("[OK]");
                    break;
                case CommandsConst.LIST_OF_TASKS:
                    System.out.println("[TASK LIST]");
                    taskService.list();
                    break;
                case CommandsConst.CLEAR_ALL_TASKS:
                    taskService.clear();
                    System.out.println("[ALL TASKS REMOVED]");
                    break;
                case CommandsConst.REMOVE_TASK:
                    System.out.println("ENTER NAME:");
                    taskName = in.nextLine();
                    taskService.remove(taskName);
                    System.out.println("[TASK " + taskName + " REMOVED]");
                    break;
                case CommandsConst.SHOW_ALL_TASKS_OF_PROJECT:
                    System.out.println("ENTER NAME OF PROJECT:");
                    projectName = in.nextLine();
                    System.out.println("PROJECT " + projectName + " CONTAINS TASKS:");
                    for (String nameOfTask : taskService.showAllTasksOfProject(projectName)) {
                        System.out.println(nameOfTask);}
                    break;
                case CommandsConst.EXIT:
                    System.exit(0);
                    break;
                default:
                    System.out.println("WRONG COMMAND. ENTER 'HELP' TO GET ALL AVAILABLE COMMANDS.");
                    break;
            }
        }
    }

}
