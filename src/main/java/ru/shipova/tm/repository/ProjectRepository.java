package ru.shipova.tm.repository;

import ru.shipova.tm.entity.Project;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projectMap = new HashMap<>();
    private List<String> listOfCommands = new ArrayList<>();

    public ProjectRepository() {
        listOfCommands.add("project-create: Create new project");
        listOfCommands.add("project-list: Show all projects");
        listOfCommands.add("project-clear: Remove all projects");
        listOfCommands.add("project-remove: Remove selected project");
        listOfCommands.add("project-update: update name of project");
    }

    public Project findOne(String projectId) {
        if (!projectMap.containsKey(projectId)) return null;
        return projectMap.get(projectId);
    }

    public List<Project> findAll() {
        List<Project> allProjects = new ArrayList<>();
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            allProjects.add(entry.getValue());
        }
        return allProjects;
    }

    public String getProjectIdByName(String projectName){
        Iterator<Map.Entry<String, Project>> iterator = projectMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Project> entry = iterator.next();
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public void persist(String projectName) {
        String projectId = UUID.randomUUID().toString();
        if (findOne(getProjectIdByName(projectName)) == null) {
            projectMap.put(projectId, new Project(projectId, projectName));
        }
    }

    public void merge(Project project) {
        if (findOne(project.getId()) == null) {
            projectMap.put(project.getId(), project);
        } else {
            update(project);
        }
    }

    public void remove(String projectId) {
        if (findOne(projectId) != null) {
            projectMap.remove(projectId);
        }
    }

    public void removeAll() {
        projectMap.clear();
    }

    private void update(Project project) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (project.getId().equals(entry.getKey())) {
                entry.getValue().setName(project.getName());
                entry.getValue().setDescription(project.getDescription());
                entry.getValue().setDateOfBegin(project.getDateOfBegin());
                entry.getValue().setDateOfEnd(project.getDateOfEnd());
            }
        }
    }

    public void list(){
        System.out.println("[PROJECT LIST]");

        Iterator<Map.Entry<String, Project>> iterator = projectMap.entrySet().iterator();
        int i = 1;

        while (iterator.hasNext()) {
            Map.Entry<String, Project> entry = iterator.next();
            String name = entry.getValue().getName();
            System.out.println(i + ": " + name);
            i++;
        }
    }

    public void printAllCommands(){
        for (String command : listOfCommands) {
            System.out.println(command);
        }
    }
}
