package ru.shipova.tm.repository;

import ru.shipova.tm.entity.Task;

import java.util.*;

public class TaskRepository {
    private Map<String, Task> taskMap = new HashMap<>();
    private List<String> listOfCommands = new ArrayList<>();

    public TaskRepository() {
        listOfCommands.add("task-create: Create new task");
        listOfCommands.add("task-list: Show all tasks");
        listOfCommands.add("task-clear: Remove all tasks");
        listOfCommands.add("task-remove: Remove selected task");
        listOfCommands.add("task-update: update name of task");
        listOfCommands.add("project-show-tasks: show all tasks of project");
    }

    public Task findOne(String taskId) {
        if (!taskMap.containsKey(taskId)) return null;
        return taskMap.get(taskId);
    }

    public List<Task> findAll() {
        List<Task> allTasks = new ArrayList<>();
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            allTasks.add(entry.getValue());
        }
        return allTasks;
    }

    public String getTaskIdByName(String taskName){

        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (taskName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * Метод вставляет новый объект, если его не было. Данные объекта не обновляет и не перезатирает.
     *
     * @param task - объект, который необходимо вставить
     */
    public void persist(Task task) {
        if (findOne(task.getId()) == null) {
            taskMap.put(task.getId(), task);
        }
    }

    public void persist(String taskName, String projectId){
        String id = UUID.randomUUID().toString();
        if (findOne(getTaskIdByName(taskName)) == null){
            taskMap.put(id, new Task(id, taskName, projectId));
        }
    }

    /**
     * Метод вставляет новый объект, если его не было.
     * Если объект был, он его обновляет.
     *
     * @param task - объект, который необходимо вставить
     */
    public void merge(Task task) {
        if (findOne(task.getId()) == null) {
            taskMap.put(task.getId(), task);
        } else {
            update(task);
        }
    }

    public void remove(String taskId) {
        if (findOne(taskId) != null) {
            taskMap.remove(taskId);
        }
    }

    public void removeAll() {
        taskMap.clear();
    }

    /**
     * Метод находит объект с идентичным id и обновляет все его поля на поля объекта task.
     *
     * @param task - объект с новыми данными.
     */
    private void update(Task task) {
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (task.getId().equals(entry.getKey())) {
                entry.getValue().setName(task.getName());
                entry.getValue().setDescription(task.getDescription());
                entry.getValue().setProjectId(task.getProjectId());
                entry.getValue().setDateOfBegin(task.getDateOfBegin());
                entry.getValue().setDateOfEnd(task.getDateOfEnd());
            }
        }
    }

    public List<String> showAllTasksOfProject(String projectId) {
        List<String> listOfTasks = new ArrayList<>();

        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (projectId.equals(entry.getValue().getProjectId())) {
                listOfTasks.add(entry.getValue().getName());
            }
        }
        return listOfTasks;
    }

    public void list() {
        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        int i = 1;

        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            String name = entry.getValue().getName();
            System.out.println(i + ": " + name);
            i++;
        }
    }

    public void removeAllTasksOfProject(String projectId) {
        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (projectId.equals(entry.getValue().getProjectId())) {
                iterator.remove();
            }
        }
    }

    public void printAllCommands() {
        for (String command : listOfCommands) {
            System.out.println(command);
        }
    }
}
