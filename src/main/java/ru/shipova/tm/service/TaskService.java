package ru.shipova.tm.service;

import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;
    private ProjectRepository projectRepository;

    public TaskService() {
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void create(String taskName, String projectName) {
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty() ||
                projectRepository.findOne(projectRepository.getProjectIdByName(projectName)) == null) return;
        taskRepository.persist(taskName, projectRepository.getProjectIdByName(projectName));
    }

    public List<String> showAllTasksOfProject(String projectName) {
        if (projectName == null || projectName.isEmpty() ||
                projectRepository.findOne(projectRepository.getProjectIdByName(projectName)) == null) return null;
        String projectId = projectRepository.getProjectIdByName(projectName);
        return taskRepository.showAllTasksOfProject(projectId);
    }

    public void list() {
        taskRepository.list();
    }

    public void clear() {
        taskRepository.removeAll();
    }

    public void remove(String taskName) {
        if (taskName == null || taskName.isEmpty() ||
                taskRepository.findOne(taskRepository.getTaskIdByName(taskName)) == null) return;
        taskRepository.remove(taskRepository.getTaskIdByName(taskName));
    }

    public void printAllCommands() {
        taskRepository.printAllCommands();
    }
}
