package ru.shipova.tm.service;

import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;

public class ProjectService {
    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService() {
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void create(String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        projectRepository.persist(projectName);
    }

    public void list() {
        projectRepository.list();
    }

    public void clear() {
        projectRepository.removeAll();
    }

    public void remove(String projectName) {
        if (projectName == null || projectName.isEmpty() ||
            projectRepository.findOne(projectRepository.getProjectIdByName(projectName)) == null) return;
        projectRepository.remove(projectRepository.getProjectIdByName(projectName));
        taskRepository.removeAllTasksOfProject(projectRepository.getProjectIdByName(projectName));
    }

    public void printAllCommands() {
        projectRepository.printAllCommands();
    }
}
