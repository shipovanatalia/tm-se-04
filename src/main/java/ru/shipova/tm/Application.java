package ru.shipova.tm;

import ru.shipova.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
